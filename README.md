# Laravel
## Sanctum, Socialite

Laravel ที่ติดตั้ง Sanctum, Socialite เพื่อให้สามารถใช้งาน Authentication ผ่าน API 
มีทั้งหมด 2 รูปแบบ
1. สมัครสมาชิกและล็อคอินผ่าน API
2. ล็อคอินผ่าน OAuth 2 ของ Social Account ต่างๆ เช่น Google, Facebook, Twitter

ทั้งสองวิธีข้างต้นจะได้รับ access_token กลับไป เมื่อ Request ให้แนบ Authorization: Bearer {$access_token} ใน headers เพื่อระบุตัวตน

วิธีใช้งาน
1. clone this repo
2. composer install
3. Configure ENV (ใส่ค่า Social Media ตามที่ต้องการจะเปิดใช้งาน check config/services.php)
 - general config
 - oauth cofig
4. php artisan migrate:fresh --seed
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Requests\Auth\RegisterRequest;

class AuthController extends Controller
{
    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'firstname' => $request->firstname,
            'surname' => $request->surname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'gender' => $request->gender,
            'age' => $request->age,
            'date_of_birth' => $request->date_of_birth,
            'address' => $request->address,
            'mobile_number' => $request->mobile_number,
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'code' => 200,
            'data' => [
                'access_token' => $token,
                'token_type' => 'Bearer',
            ],
        ]);
    }

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                // 'message' => 'Invalid credentials.'
                'message' => 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง'
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;
        // $token = $user->createToken('mobile_token', ['server:read', 'server:create', 'server:update', 'server:delete'])->plainTextToken;

        return response()->json([
            'code' => 200,
            'data' => [
                'access_token' => $token,
                'token_type' => 'Bearer',
            ],
        ]);
    }

    public function revoke(Request $request)
    {
        return $request->user()->tokens()->where('id', $request->user()->currentAccessToken()->id)->delete();
    }

    public function loginProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function loginCallback($provider)
    {
        $socialite = Socialite::driver($provider)->user();

        $user = User::where("{$provider}_id", $socialite->id)->first();

        if ($user) {
            $user->update([
                "avatar" => $socialite->avatar,
                "{$provider}_token" => $socialite->token,
                "{$provider}_refresh_token" => $socialite->refreshToken,
            ]);
        } else {
            $name = explode(" ", $socialite->name);

            $user = User::create([
                "firstname" => $name[0],
                "surname" => $name[1],
                "email" => $socialite->email,
                "avatar" => $socialite->avatar,
                "{$provider}_id" => $socialite->id,
                "{$provider}_token" => $socialite->token,
                "{$provider}_refresh_token" => $socialite->refreshToken,
            ]);
        }

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'code' => 200,
            'data' => [
                'access_token' => $token,
                'token_type' => 'Bearer',
            ],
        ]);
    }
}

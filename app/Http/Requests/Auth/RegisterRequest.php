<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'firstname' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'gender' => 'nullable|in:M,F',
            'age' => 'nullable|integer|max:150',
            'date_of_birth' => 'nullable|date_format:Y-m-d',
            'address' => 'nullable|string',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'code'  => 400,
            'message' => 'Bad Request',
            'data' => null,
            'validator' => $validator->errors()
        ]));
    }
}

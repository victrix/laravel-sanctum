<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// http://localhost:1002/api/login/google

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::get('/login/{provider}', [AuthController::class, 'loginProvider']);
Route::get('/login/{provider}/callback', [AuthController::class, 'loginCallback']);

Route::post('/tokens/revoke', [AuthController::class, 'revoke'])->middleware('auth:sanctum');

// has at least one of the listed abilities
// Route::get('/articles', [ArticleController::class, 'index'])->middleware(['auth:sanctum', 'ability:server:update,server:create']);

// has all of the listed abilities
// Route::get('/articles', [ArticleController::class, 'index'])->middleware(['auth:sanctum', 'abilities:check-status,place-orders']); 

Route::get('/articles', [ArticleController::class, 'index']);